<?php
App::uses('AppController', 'Controller');
/**
 * Resumes Controller
 *
 * @property Resume $Resume
 * @property PaginatorComponent $Paginator
 */
class ResumesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'RequestHandler');

	// public function beforeFilter() {
	// 	$this->Auth->allow('add');
	// }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Resume->recursive = 0;
		$options = array('Resume.user_id' => $this->Session->read('Auth.User.id'));
		$this->set('resumes', $this->Paginator->paginate($options));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Resume->exists($id)) {
			throw new NotFoundException(__('Invalid resume'));
		}


		$log_user_id = $this->Session->read('Auth.User.id');
		$log_name = $this->Session->read('Auth.User.name');
		$log_mobile = $this->Session->read('Auth.User.mobile');
		$log_email = $this->Session->read('Auth.User.email');
		$log_education = $this->Session->read('Auth.User.education');
		$log_institute = $this->Session->read('Auth.User.institute');
		$log_grade = $this->Session->read('Auth.User.grade');

		$this->set('log_name',$log_name);
		$this->set('log_mobile',$log_mobile);
		$this->set('log_email',$log_email);
		$this->set('log_education',$log_education);
		$this->set('log_institute',$log_institute);
		$this->set('log_grade',$log_grade);



		$options = array('conditions' => array('Resume.' . $this->Resume->primaryKey => $id));

		$this->pdfConfig = array(
			'download' => true,
			'filename' => $log_name .'.pdf'
		);

		$this->set('resume', $this->Resume->find('first', $options));


	}

	public function view_old($id = null) {
		if (!$this->Resume->exists($id)) {
			throw new NotFoundException(__('Invalid resume'));
		}
		$options = array('conditions' => array('Resume.' . $this->Resume->primaryKey => $id));
		$this->set('resume', $this->Resume->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		$log_user_id = $this->Session->read('Auth.User.id');
		$log_name = $this->Session->read('Auth.User.name');
		$log_mobile = $this->Session->read('Auth.User.mobile');
		$log_email = $this->Session->read('Auth.User.email');
		$log_education = $this->Session->read('Auth.User.education');
		$log_institute = $this->Session->read('Auth.User.institute');
		$log_grade = $this->Session->read('Auth.User.grade');

		$this->set('log_name',$log_name);
		$this->set('log_mobile',$log_mobile);
		$this->set('log_email',$log_email);
		$this->set('log_education',$log_education);
		$this->set('log_institute',$log_institute);
		$this->set('log_grade',$log_grade);

		$resume = $this->Resume->find('first', array('order' => array('Resume.id' => 'desc'), 'conditions' => array('Resume.user_id' => $log_user_id), 'limit' => 1));
		// echo "User_Id: ".$log_user_id;
		// var_dump($resume);
		// echo $resume_id;
		// die();
		if (empty($resume)) {
			$resume_id = "";
			$this->set('log_resume_id',$resume_id);
		}else{
			$resume_id = $resume['Resume']['id'];
			$this->set('log_resume_id',$resume_id);			
		}
		// die();
		$this->request->data['Resume']['user_id'] = $log_user_id;

		if ($this->request->is('post')) {

			// $this->request->data['Resume']['template'] = $_SESSION['template'];
			$this->request->data['Resume']['user_id'] = $log_user_id;

			// var_dump($this->request->data);
			// die();

			$this->Resume->create();
			if ($this->Resume->save($this->request->data)) {
				$this->Session->setFlash(__('The resume has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The resume could not be saved. Please, try again.'));
			}
		}
		$users = $this->Resume->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Resume->exists($id)) {
			throw new NotFoundException(__('Invalid resume'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Resume->save($this->request->data)) {
				$this->Session->setFlash(__('The resume has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The resume could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Resume.' . $this->Resume->primaryKey => $id));
			$this->request->data = $this->Resume->find('first', $options);
		}
		$users = $this->Resume->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Resume->id = $id;
		if (!$this->Resume->exists()) {
			throw new NotFoundException(__('Invalid resume'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Resume->delete()) {
			$this->Session->setFlash(__('The resume has been deleted.'));
		} else {
			$this->Session->setFlash(__('The resume could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function newadd() {

		$log_user_id = $this->Session->read('Auth.User.id');
		$log_name = $this->Session->read('Auth.User.name');
		$log_mobile = $this->Session->read('Auth.User.mobile');
		$log_email = $this->Session->read('Auth.User.email');
		$log_education = $this->Session->read('Auth.User.education');
		$log_institute = $this->Session->read('Auth.User.institute');
		$log_grade = $this->Session->read('Auth.User.grade');

		$this->set('log_name',$log_name);
		$this->set('log_mobile',$log_mobile);
		$this->set('log_email',$log_email);
		$this->set('log_education',$log_education);
		$this->set('log_institute',$log_institute);
		$this->set('log_grade',$log_grade);

		$resume = $this->Resume->find('first', array('order' => array('Resume.id' => 'desc'), 'conditions' => array('Resume.user_id' => $log_user_id), 'limit' => 1));
		// echo "User_Id: ".$log_user_id;
		// var_dump($resume);
		// echo $resume_id;
		// die();
		if (empty($resume)) {
			$resume_id = "";
			$this->set('log_resume_id',$resume_id);
		}else{
			$resume_id = $resume['Resume']['id'];
			$this->set('log_resume_id',$resume_id);			
		}
		// die();


		if ($this->request->is('post')) {

			// $this->request->data['Resume']['template'] = $_SESSION['template'];
			$this->request->data['Resume']['user_id'] = $log_user_id;

			// var_dump($this->request->data);
			// die();

			$this->Resume->create();
			if ($this->Resume->save($this->request->data)) {
				$this->Session->setFlash(__('The resume has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The resume could not be saved. Please, try again.'));
			}
		}
		$users = $this->Resume->User->find('list');
		$this->set(compact('users'));		
	}

}
