<!-- logout tab - #5584b8 -->
<!-- Template tab - #365d84 -->
<!-- Edit tab - #22394b -->
<!-- Pandhra - #ffffff -->
<!-- background - #f4f4f4 -->
<div style="padding: 0%; height: 100%; width: 100%; margin: 0%;">


<div class="left_div">
	<div class="logo_div">
		<p class="temp_theme">Current Theme</p>
		<b class="temp_head">Template</b>
	</div>
	<div class="edit_div">
		<p class="temp_edit">EDIT</p>
	</div>
	<div class="temp_div">
		<table style="height: 100%;">
			<tr>
				<td style="width: 50%;">
		    		<a href="#" id="first_template_link">
						<div style="margin-left: 25px; background-color: #e0e0e0; width: 125px; height: 130px;"></div>
						<div style="margin-left: 25px; width: 125px; height: 30px; clear: both; text-align: center;">Template 1</div>
					</a>
				</td>
				<td style="width: 50%;">
					<a href="#" id="second_template_link">
						<div style="margin-left: 25px; background-color: #e0e0e0; width: 125px; height: 130px;"></div>
						<div style="margin-left: 25px; width: 130px; height: 30px; clear: both; text-align: center;">Template 2</div>
					</a>
				</td>
			</tr>
		</table>

	</div>

	<div class="form_div">
<?php

		echo $this->Form->create('Resume');
		echo $this->Form->input('user_id', array('type' => 'hidden'));
		echo $this->Form->input('template', array('type' => 'hidden', 'id' => 'template_field'));
		// echo $this->Form->input('qualification', array('id' => 'qual', 'onclick' => 't1();'));
		echo $this->Form->input('experience', array('id' => 'exp', 'onkeypress' => 't1();'));
		echo $this->Form->input('projects', array('id' => 'proj', 'onkeypress' => 't1();'));	
		echo $this->Form->input('skills', array('id' => 'ski', 'onkeypress' => 't2();'));
		echo $this->Form->input('hobbies', array('id' => 'hob', 'onkeypress' => 't3();'));
		echo $this->Form->end('Save');
?>
	</div>
</div>
<div class="right_div">
	<div class="btn_div">
		<div style="background-color: #363269; color: #ffffff; height: 30px; margin-top: 10px; display: inline-block; float: right;">
			<span style="color: #ffffff;"><?php 
				if ($log_resume_id == "") {
					// echo $this->Html->link(__('Download'), array('id' => 'no_resume', 'return false;')); 
				?>
					<a href="#" id="no_resume" style="color: #ffffff;" class="btn btn-primary" return false;>Download</a>
				<?php	
				}else{
					echo $this->Html->link(__('Download'), array('action' => 'view', $log_resume_id, 'ext' => 'pdf'), array('class' => 'btn btn-primary'));
				}
				?>
			</span>
		</div>
		<div style="margin-top: 15px; margin-right: 10px; display: inline-block; float: right;">
			<span style=""><?php echo $this->Html->link(__('My Saved'), array('action' => 'index')); ?></span>
		</div>
	</div>
	<div class="resume_div">

			<div id="template_1" class="cv">
				<center><p>Template 1</p></center>
				<div class="personal_details">
					<b>Personal Details:</b><br><br><br>
					<p id="name" style="margin-top: 10px;"><?php echo $log_name; ?></p>
					<span id="education"><?php echo $log_education; ?> - </span><span><?php echo $log_grade; ?></span><br><br><br>
					<p id="institute" style="margin-top: 10px;"><?php echo $log_institute; ?></p>
					<p><?php echo $log_mobile; ?></p>
					<p><?php echo $log_email; ?></p>
				</div>

				<div class="projects_worked_and_experience">
					<b>Experience And Projects Worked On:</b><br>
					<p id="experience"></p>
					<p id="projects"></p>

				</div>

				<div class="skills_div">
					<b>Skills:</b><br><br>
					<p id="skills"></p>
				</div>

				<div class="hobbies_div">
					<b>Hobbies:</b><br><br>
					<p id="hobbies"></p>
				</div>

				<div class="undertaking_div">
					<p id="undertaking">The information provided above is true as per my knowledge!</p>
					<p>Date:</p>
					<p>Signature</p>
				</div>

			</div>

			<div id="template_2" class="cv">
				<center><p>Template 2</p></center>
				<div class="personal_details_2">
					<b>Personal Details:</b><br>
					<p id="name2" style="margin-top: 10px;"><?php echo $log_name; ?></p>
					<span id="education2"><?php echo $log_education; ?> - </span><span><?php echo $log_grade; ?></span><br><br><br>
					<p id="institute2"><?php echo $log_institute; ?></p>
					<p><?php echo $log_mobile; ?></p>
					<p><?php echo $log_email; ?></p>
				</div>

				<div class="projects_worked_and_experience_2">
					<b>Experience And Projects Worked On:</b><br>
					<p id="experience2"></p>
					<p id="projects2"></p>

				</div>

				<div class="skills_div_2">
					<b>Skills:</b><br>
					<p id="skills2"></p>
				</div>

				<div class="hobbies_div_2">
					<b>Hobbies:</b><br>
					<p id="hobbies2"></p>
				</div>

				<div class="undertaking_div_2">
					<p id="undertaking">The information provided above is true as per my knowledge!</p>
					<p>Date:</p>
					<p>Signature</p>
				</div>
			</div>


	</div>
	<!-- RESUME DIV ENDS HERE	 -->
</div>


</div>