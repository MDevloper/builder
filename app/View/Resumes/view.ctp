	<div class="t1">

<!-- TEMPLATE 1 CODE -->
<?php
	if ($resume['Resume']['template'] == "template1") {
?>
			<div class="personal_details">
				<center>Basic Resume - Template 1</center><p></p>
				<b>Personal Details:</b>
				<p id="name" style="margin-top: 10px;"><?php echo $log_name; ?></p><br><p></p>
				<span id="education" style="margin-top: 10px; padding-top: 10px;"><?php echo $log_education; ?> - </span><span><?php echo $log_grade; ?></span><br><p></p>
				<p id="institute" style="margin-top: 10px;"><?php echo $log_institute; ?></p>
				<p><?php echo $log_mobile; ?></p>
				<p><?php echo $log_email; ?></p><p></p><p></p>
			</div>

			<div class="projects_worked_and_experience">
				<b>Experience And Projects Worked On:</b>
				<p id="experience2">
					<?php echo $resume['Resume']['experience']; ?>
				</p>
				<p id="projects2">
					<?php echo $resume['Resume']['projects']; ?>
				</p>

			</div>

			<div class="skills_div">
				<b>Skills:</b>
				<p id="skills2">
					<?php echo $resume['Resume']['skills']; ?>
				</p>
			</div>

			<div class="hobbies_div">
				<b>Hobbies:</b>
				<p id="hobbies2">
					<?php echo $resume['Resume']['hobbies']; ?>
				</p>
			</div>

			<div class="undertaking_div">
				<p id="undertaking">The information provided above is true as per my knowledge!</p>
				<p>Date:</p>
				<p>Signature</p>
			</div>

<?php
}else{
?>
<!-- TEMPLATE 1 CODE ENDS HERE -->
<!-- TEMPLATE 2 CODE -->

			<div class="personal_details_2">
				<center>Basic Resume - Template 2</center><p></p>
				<b>Personal Details:</b>
				<p id="name2" style="margin-top: 10px;"><?php echo $log_name; ?></p><br><p></p>
				<span id="education2" style="margin-top: 10px; padding-top: 10px;"><?php echo $log_education; ?> - </span><span><?php echo $log_grade; ?></span><br><p></p>
				<p id="institute2"><?php echo $log_institute; ?></p>
				<p><?php echo $log_mobile; ?></p>
				<p><?php echo $log_email; ?></p>
			</div>

			<div class="projects_worked_and_experience_2">
				<b>Experience And Projects Worked On:</b>
				<p id="experience2">
					<?php echo $resume['Resume']['experience']; ?>
				</p>
				<p id="projects2">
					<?php echo $resume['Resume']['projects']; ?>
				</p>

			</div>

			<div class="skills_div_2">
				<b>Skills:</b>
				<p id="skills2">
					<?php echo $resume['Resume']['skills']; ?>
				</p>
			</div>

			<div class="hobbies_div_2">
				<b>Hobbies:</b>
				<p id="hobbies2">
					<?php echo $resume['Resume']['hobbies']; ?>
				</p>
			</div>

			<div class="undertaking_div_2">
				<p id="undertaking">The information provided above is true as per my knowledge!</p>
				<p>Date:</p>
				<p>Signature</p>
			</div>
<?php
}
?>
<!-- TEMPLATE 2 CODE ENDS HERE -->
		</div>	