	<div class="t1">

<!-- TEMPLATE 1 CODE -->
<?php
	if ($resume['Resume']['template'] == "template1") {
?>
			<div class="personal_details">
				<center>Basic Resume - Template 1</center>
				<h2><b>Personal Details:</b></h2>
				<p id="name" style="margin-top: 10px;"><?php echo $log_name; ?></p><br>
				<span id="education" style="margin-top: 10px; padding-top: 10px;"><?php echo $log_education; ?> - </span><span><?php echo $log_grade; ?></span><br>
				<p id="institute" style="margin-top: 10px;"><?php echo $log_institute; ?></p>
				<p><?php echo $log_mobile; ?></p>
				<p><?php echo $log_email; ?></p>
			</div>

			<div class="projects_worked_and_experience">
				<h2><b>Experience And Projects Worked On:</b></h2>
				<p id="experience2">
					<?php echo $resume['Resume']['experience']; ?>
				</p>
				<p id="projects2">
					<?php echo $resume['Resume']['projects']; ?>
				</p>

			</div>

			<div class="skills_div">
				<h2><b>Skills:</b></h2>
				<p id="skills2">
					<?php echo $resume['Resume']['skills']; ?>
				</p>
			</div>

			<div class="hobbies_div">
				<h2><b>Hobbies:</b></h2>
				<p id="hobbies2">
					<?php echo $resume['Resume']['hobbies']; ?>
				</p>
			</div>

			<div class="undertaking_div">
				<p id="undertaking">The information provided above is true as per my knowledge!</p>
				<p>Date:</p>
				<p>Signature</p>
			</div>

<?php
}else{
?>
<!-- TEMPLATE 1 CODE ENDS HERE -->
<!-- TEMPLATE 2 CODE -->

			<div class="personal_details_2">
				<center>Basic Resume - Template 2</center>
				<h2><b>Personal Details:</b></h2>
				<p id="name2" style="margin-top: 10px;"><?php echo $log_name; ?></p><br>
				<span id="education2" style="margin-top: 10px; padding-top: 10px;"><?php echo $log_education; ?> - </span><span><?php echo $log_grade; ?></span><br>
				<p id="institute2"><?php echo $log_institute; ?></p>
				<p><?php echo $log_mobile; ?></p>
				<p><?php echo $log_email; ?></p>
			</div>

			<div class="projects_worked_and_experience_2">
				<h2><b>Experience And Projects Worked On:</b></h2>
				<p id="experience2">
					<?php echo $resume['Resume']['experience']; ?>
				</p>
				<p id="projects2">
					<?php echo $resume['Resume']['projects']; ?>
				</p>

			</div>

			<div class="skills_div_2">
				<h2><b>Skills:</b></h2>
				<p id="skills2">
					<?php echo $resume['Resume']['skills']; ?>
				</p>
			</div>

			<div class="hobbies_div_2">
				<h2><b>Hobbies:</b></h2>
				<p id="hobbies2">
					<?php echo $resume['Resume']['hobbies']; ?>
				</p>
			</div>

			<div class="undertaking_div_2">
				<p id="undertaking">The information provided above is true as per my knowledge!</p>
				<p>Date:</p>
				<p>Signature</p>
			</div>
<?php
}
?>
<!-- TEMPLATE 2 CODE ENDS HERE -->
		</div>	