<div class="resumes view">
<h2><?php echo __('Resume'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resume['User']['name'], array('controller' => 'users', 'action' => 'view', $resume['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Experience'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['experience']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Projects'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['projects']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Skills'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['skills']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hobbies'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['hobbies']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Template'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['template']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resume'), array('action' => 'edit', $resume['Resume']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resume'), array('action' => 'delete', $resume['Resume']['id']), array(), __('Are you sure you want to delete # %s?', $resume['Resume']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Resumes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resume'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
