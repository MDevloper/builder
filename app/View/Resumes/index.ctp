<div class="action">
<!-- <div class="new_action"> -->
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Resume'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?></li>
	</ul>
</div>
<div class="resumes index">
	<h2><?php echo __('Resumes'); ?></h2>


	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('experience'); ?></th>
			<th><?php echo $this->Paginator->sort('projects'); ?></th>
			<th><?php echo $this->Paginator->sort('skills'); ?></th>
			<th><?php echo $this->Paginator->sort('hobbies'); ?></th>
			<th><?php echo $this->Paginator->sort('template'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($resumes as $resume): ?>
	<tr>
		<td><?php echo h($resume['Resume']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($resume['User']['name'], array('controller' => 'users', 'action' => 'view', $resume['User']['id'])); ?>
		</td>
		<td><?php echo h($resume['Resume']['experience']); ?>&nbsp;</td>
		<td><?php echo h($resume['Resume']['projects']); ?>&nbsp;</td>
		<td><?php echo h($resume['Resume']['skills']); ?>&nbsp;</td>
		<td><?php echo h($resume['Resume']['hobbies']); ?>&nbsp;</td>
		<td>
			<?php 
			echo h($resume['Resume']['template']); ?>
			<?php 
			// echo $this->Html->link($resume['Resume']['template'], array('controller' => 'resumes', 'action' => 'view', $resume['Resume']['id'], 'ext' => 'pdf')); ?>&nbsp;
		</td>
		<td><?php echo h($resume['Resume']['created']); ?>&nbsp;</td>
		<td><?php echo h($resume['Resume']['modified']); ?>&nbsp;</td>
		<td class="actions">
<!-- 			<?php echo $this->Html->link(__('View'), array('action' => 'view', $resume['Resume']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $resume['Resume']['id'])); ?> -->
			<?php 
			// echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $resume['Resume']['id']), array(), __('Are you sure you want to delete # %s?', $resume['Resume']['id'])); 
			echo $this->Html->link('Download', array('controller' => 'resumes', 'action' => 'view', $resume['Resume']['id'], 'ext' => 'pdf')); ?>&nbsp;
			
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>