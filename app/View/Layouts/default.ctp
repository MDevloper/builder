<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

<!-- MY CUSTOM JQUERY CODE -->

<?php echo $this->Html->script(array('animate','bootstrap','bootstrap.min','classie','custom','modernizr'));?>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	
	<script type="text/javascript">
	
		$(document).ready(function() {

			// function noresume(){
			// 	alert("No Resume Saved Previously, Kindly Save Resume And Try Again!");
			// }
			$('#no_resume').click(function() {
				alert("No Resume Saved Previously, Kindly Save Resume And Try Again!");
			});

			$("#template_1").show();
			$("#template_2").hide();

        	$("#template_field").val("template1");
			
	        $("#first_template_link").click(function(){
	        	// alert("First Temp");
	        	$("#template_2").hide();
	        	$("#template_1").show();
				// setSession('template', "template1");
	        	$("#template_field").val("template1");
	        });

	        $("#second_template_link").click(function(){
	        	// alert("Second Temp");
	        	$("#template_1").hide();
	        	$("#template_2").show();
				// setSession('template', "template2");
	        	$("#template_field").val("template2");
	        });

	        $("#exp").keyup(function(){
	        	$("#experience").html($(this).val());
	        	$("#experience2").html($(this).val());
	        });

	        $("#proj").keyup(function(){
	        	$("#projects").html($(this).val());
	        	$("#projects2").html($(this).val());
	        });

	        $("#ski").keyup(function(){
	        	$("#skills").html($(this).val());
	        	$("#skills2").html($(this).val());
	        });

	        $("#hob").keyup(function(){
	        	$("#hobbies").html($(this).val());
	        	$("#hobbies2").html($(this).val());
	        });

		});


	</script>

<!-- MY CUSTOM JQUERY CODE ENDS HERE -->

	<title>
		<?php
		 // echo $cakeDescription 
		 ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>

<!-- NEW THEME CSS -->
<?php echo $this->Html->css(array('bootstrap','bootstrap.min','bootstrap-responsive','bootstrap-responsive.min','cake.generic','docs','style'));?>
<!-- NEW THEME CSS ENDS HERE -->

<!-- MY CUSTOM STYLE CLASSES -->
<style type="text/css">
	.d1{
		height: 75px;
		width: 350px;
		border: 1px solid black;
		display: inline-block;
		margin-right: 0px;
		vertical-align: text-bottom;
		background-color: #3949ab;
		color: white;
	}
	.d2{
		height: 75px;
		width: 652px;
		/*border: 1px solid black;*/
		/*display: inline-block;*/
		vertical-align: top;
		float: right;
		margin-bottom: 0px;
		background-color: white;
	}
	.d3{
		/*clear: both;*/
		height: 625px;
		width: 660px;
		/*border: 1px solid black;*/
		margin-right: 0px;
		/*position: absolute;*/
		float: right;
		background-color: #eae4e4;
	}
	.d4{
		float: left;
		height: 40px;
		width: 352px;
		/*border: 1px solid black;*/
		right: 0px;
		margin-top: 0px;
		background-color: #2e265f;
		color: white;
	}
	.d5{
		height: 582px;
		width: 350px;
		float: left;
		/*border: 1px solid black;*/
		overflow-y: scroll;
		background-color: white;
	}
	.cv{
		width: 600px;
		height: 850px;
		margin: 0 auto;
		margin-top: 40px;
		/*border: 1px solid black;*/
		background-color: white;
		color: black;
		padding-left: 10px;
	}
	.temp{
		display: inline-block;
		height: 100px;
		width: 100px;
		margin: 10px 30px auto;
		/*border: 1px solid black;*/
		text-align: center;
		background-color: #d6d6d6;
	}
	.d6{
		vertical-align: bottom;
		height: 20px;
		/*position: absolute;*/
		margin-bottom: 0px;
	}
	.personal_details{
		width: 90%;
		height: 20%;
		line-height: 5px;
		margin: 0 auto;
		line-height: 15px
		font-size: 25px
		/*border: 1px solid black;*/
		margin-top: 20px;
	}
	.projects_worked_and_experience{
		width: 90%;
		height: 20%;
		margin: 0 auto;
		margin-top: 20px;
		line-height: 15px
		/*border: 1px solid black;*/
	}
	.skills_div{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px
		/*border: 1px solid black;*/
	}
	.hobbies_div{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px;
		/*border: 1px solid black;*/
	}
	.undertaking_div{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px;
		/*border: 1px solid black;*/
	}
	.personal_details_2{
		width: 90%;
		height: 20%;
		margin: 0 auto;
		line-height: 15px;
		/*border: 1px solid black;*/
		text-align: center;
		margin-top: 20px;
	}
	.projects_worked_and_experience_2{
		width: 90%;
		height: 20%;
		margin: 0 auto;
		margin-top: 20px;
		/*border: 1px solid black;*/
		text-align: center;
		line-height: 15px;
	}
	.skills_div_2{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		/*border: 1px solid black;*/
		text-align: center;
		line-height: 15px;

	}
	.hobbies_div_2{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		/*border: 1px solid black;*/
		text-align: center;
		line-height: 15px;
	}
	.undertaking_div_2{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px;
		/*text-align: center;*/
		/*border: 1px solid black;*/
	}
	.t1{
		width: 700px;
		height: 950px;
		margin: 0 auto;
		margin-top: 40px;
		border: 1px solid black;
		background-color: white;
		color: black;
		padding-left: 10px;
		font-size: 25px
	}
	.left_div{
		width: 30%;
		height: 950px;
		/*border: 1px solid black;*/
		float: left;
	}
	.right_div{
		width: 69%;
		height: 950px;
		/*border: 1px solid black;*/
		float: right;
	}
	.logo_div{
		width: 100%;
		height: 10%;
		background-color: #365d84;
		color: #ffffff;
		/*border: 1px solid black;*/
	}
	.edit_div{
		width: 100%;
		height: 5%;
		background-color: #22394b;
		color: #ffffff;
		/*border: 1px solid black;		*/
	}
	.temp_div{
		width: 100%;
		height:22%;
		/*border: 1px solid black;*/
	}
	.template_link{
		display: inline-block;
		height: 150px;
		width: 100px;
		margin: 10px 30px auto;
		/*border: 1px solid black;*/
		text-align: center;
		background-color: #d6d6d6;
	}
	.form_div{
		width: 100%;
		height:62%;
		overflow-y: scroll;
		/*border: 1px solid black;*/
	}
	.btn_div{
		width: 100%;
		height: 5%;
		background-color: #ffffff;
		/*border: 1px solid black;*/
	}
	.resume_div{
		width: 100%;
		height: 95%;
		background-color: #f4f4f4;
		padding-top: 20px;
		/*border: 1px solid black;		*/
	}
	.temp_theme{
		padding-top: 20px;
		padding-left: 30px;
	}
	.temp_head{
		font-size: 20px !important;
		padding-left: 30px;
	}
	.temp_edit{
		padding-top: 15px;
		padding-left: 30px;
		font-weight: bold !important;
	}
	.q_logo{
		padding-top: 15px;
		font-size: 30px;
		font-weight: bolder !important;
		color: #ffffff;
	}
	.action{
		vertical-align: top;
		float: right;
	}
	input{
		border: 1px solid black !important;
		border-radius:10px !important;
		width: 300px !important;
	}
	.custom_form{
		width: 30% !important;
		height: 60% !important;
		margin: 0 auto !important;

	}
	input>button{
/*		border: 1px solid black !important;
		border-radius:10px !important;*/
		width: 30px !important;
	}
/*	.custom-class{
		width: 10% !important;
		background: #065158 !important;
		margin-right: 0px !important;
	}
	input#uname,#pword{
		margin-left: 10%;
	}*/
/*	.first_span{
		padding-top: 10px;
	}*/
</style>

<!-- MY CUSTOM STYLE CLASSES ENDS HERE -->

<body>
	<div id="container" style="background-color: #5584b8;">
		<div id="header">
			<!-- <h1> -->
				<div style="float: left; clear: left; padding-left: 0%;">
					<p class="q_logo">QuezX</p>
				</div>
				<div style="float: right;">
			<?php 
					// echo $this->Html->link($cakeDescription, 'http://cakephp.org');
				if ($this->Session->read('Auth.User.id') != "") {
				 	echo $this->Html->link(__('Log Out'), ['controller' => 'Users', 'action' => 'logout']);
	    		 }			 
			 ?>		
			 </div>				
			<!-- </h1> -->
		</div>
		<div id="content" style="padding: 0%;">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
 		<!-- <div id="footer"> -->
			<?php 
				// echo $this->Html->link(
				// 	$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
				// 	'http://www.cakephp.org/',
				// 	array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
				// );
			?>
			<!-- <p> -->
				<?php 
					// echo $cakeVersion; 
				?>
			</p>
		<!-- </div> -->
	<!-- </div> -->
	<?php
	 // echo $this->element('sql_dump'); 
	 ?>
</body>
</html>
