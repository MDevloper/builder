<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->css('style');
	?>
</head>

<!-- MY CUSTOM STYLE CLASSES -->
<style type="text/css">

	.personal_details{
		width: 90%;
		height: 25%;
		margin: 0 auto;
		line-height: 15px
		/*border: 1px solid black;*/
		margin-top: 20px;
	}
	.projects_worked_and_experience{
		width: 90%;
		height: 20%;
		margin: 0 auto;
		line-height: 15px
		/*border: 1px solid black;*/
	}
	.skills_div{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px
		/*border: 1px solid black;*/
	}
	.hobbies_div{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px;
		/*border: 1px solid black;*/
	}
	.undertaking_div{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px;
		/*border: 1px solid black;*/
	}
	.personal_details_2{
		width: 90%;
		height: 25%;
		margin: 0 auto;
		line-height: 15px;
		/*border: 1px solid black;*/
		text-align: center;
		margin-top: 20px;
	}
	.projects_worked_and_experience_2{
		width: 90%;
		height: 20%;
		margin: 0 auto;
		/*border: 1px solid black;*/
		text-align: center;
		line-height: 15px;
	}
	.skills_div_2{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		/*border: 1px solid black;*/
		text-align: center;
		line-height: 15px;

	}
	.hobbies_div_2{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		/*border: 1px solid black;*/
		text-align: center;
		line-height: 15px;
	}
	.undertaking_div_2{
		width: 90%;
		height: 15%;
		margin: 0 auto;
		line-height: 15px;
		/*text-align: center;*/
		/*border: 1px solid black;*/
	}
	.t1{
		width: 700px;
		height: 1000px;
		margin: 0 auto;
		margin-top: 40px;
		border: 1px solid black;
		background-color: white;
		color: black;
		padding-left: 10px;
		font-size: 25px
	}

/*	.first_span{
		padding-top: 10px;
	}*/
</style>

<!-- MY CUSTOM STYLE CLASSES ENDS HERE -->

<body>
	<div id="container">
		<div id="content">
			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
</body>
</html>